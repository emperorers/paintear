import java.awt.Dimension;

import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.Timer;


public class BudowniczyObraz extends Budowniczy {

	BudowniczyObraz(Scenariusz scen) {
		super(scen);
		// TODO Auto-generated constructor stub
	}
	
	Timer timer;
	Timer gtimer;
	JPlansza2 plansza;
	Kafelki matrix2;
	
	@Override
	public void budujplansze() {
		// TODO Auto-generated method stub
		plansza=new JPlansza2();
		plansza.setPreferredSize(new Dimension(scenariusz.wys*scenariusz.wyskafla, scenariusz.szer*scenariusz.szerkafla));
		 matrix2 = new Kafelki(scenariusz.wys,scenariusz.szer);

		for (int i=0; i<scenariusz.wys; i++)
		{
			for (int y=0; y<scenariusz.szer; y++)
			{
				matrix2.set(i, y, new Kafel());
				matrix2.get(i,y).setBounds(i*scenariusz.wyskafla, y*scenariusz.szerkafla, scenariusz.wyskafla-1, scenariusz.szerkafla-1);
			}
		}

		plansza.add(matrix2);
		plansza.setLayout(null);

	}

	@Override
	public void budujgracza(int i, Action wgore, Action wdol, Action wlewo, Action wprawo, Action stopwgore, Action stopwdol, Action stopwlewo, Action stopwprawo, Action odpalspec) {
		// TODO Auto-generated method stu

		Graczi g = null;
		if (i==0)
		{
		g=new Gracz1();
		g.setBounds(0, 0, scenariusz.wyskafla, scenariusz.szerkafla);
		
		}
		
		else if (i==1)
		{
			g=new Gracz2();
			g.setBounds((scenariusz.wys-1)*(scenariusz.wyskafla), ((scenariusz.szer-1)*(scenariusz.szerkafla)),  scenariusz.wyskafla, scenariusz.szerkafla);
		}
		else if (i==2)
		{
			g=new Gracz3();
			g.setBounds((scenariusz.wys-1)*(scenariusz.wyskafla), 0, scenariusz.wyskafla, scenariusz.szerkafla);			
		}
		else if (i==3)
		{
			g= new Gracz4();
			g.setBounds(-1,  ((scenariusz.szer-1)*(scenariusz.szerkafla)),  scenariusz.wyskafla, scenariusz.szerkafla);
		}
		g.setWysSzer(scenariusz.wyskafla, scenariusz.szerkafla);
		g.setBackground(scenariusz.cgraczy[i]);

		//WZORZEC POLECENIE inicjatorami sa przyciski przypisane graczowi, 
		g.getInputMapI(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("released "+ scenariusz.stergraczy[i][0]), "stopgora");
		g.getInputMapI(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("released "+ scenariusz.stergraczy[i][1]), "stopdol");
		g.getInputMapI(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("released "+ scenariusz.stergraczy[i][2]), "stoplewo");
		g.getInputMapI(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("released "+ scenariusz.stergraczy[i][3]), "stopprawo");
		g.getInputMapI(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(scenariusz.stergraczy[i][0]), "gora");
		g.getInputMapI(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(scenariusz.stergraczy[i][1]), "dol");
		g.getInputMapI(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(scenariusz.stergraczy[i][2]), "lewo");
		g.getInputMapI(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(scenariusz.stergraczy[i][3]), "prawo");
		g.getInputMapI(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(scenariusz.stergraczy[i][4]), "spec");
		
		//WZORZEC POLECENIE wykonawcami czynnosci sa akcje podane jako parametr
		g.getActionMapI().put("stopgora",stopwgore);
		g.getActionMapI().put("stopdol", stopwdol);
		g.getActionMapI().put("stoplewo", stopwlewo);
		g.getActionMapI().put("stopprawo", stopwprawo);

		g.getActionMapI().put("gora", wgore);
		g.getActionMapI().put("dol", wdol);
		g.getActionMapI().put("lewo", wlewo);
		g.getActionMapI().put("prawo", wprawo);
		g.getActionMapI().put("spec", odpalspec);

		plansza.addgracza(g);
		
	}

	@Override
	public JPlansza2 getResult() {
		// TODO Auto-generated method stub
		return plansza;
	}

	@Override
	public void budujtimer(Elementkontrprzebieg elementkontrprzebieg) {
		// TODO Auto-generated method stub
		timer = new Timer(scenariusz.sec*25, elementkontrprzebieg);
		timer.setInitialDelay(scenariusz.sec);
		timer.start();

	}
}
