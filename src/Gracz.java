import java.awt.Color;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;


//WSPOLNY INTERFEJS DLA OBIEKU DEKOROWANEGO I JESZCZE NIEUDEKOROWANEGO
 interface Graczi{
	int getpromien();
	int getspeed();
	void idz(int y);
	void setBounds(int i, int j, int wyskafla, int szerkafla);
	void setWysSzer(int wyskafla, int szerkafla);
	void setBackground(Color color);
	InputMap getInputMapI(int whenInFocusedWindow);
	ActionMap getActionMapI();
	int getX();
	int getWidth();
	int getHeight();
	Color getBackgroundColor();
	int getY();
	void setBackgroundColor(Color color);

	
}
//ELEMENTY DEKOROWANE
public class Gracz extends JComponent implements Graczi {

	boolean gora=false,dol=false, prawo=false, lewo=false;
	int wys;
	int szer;
	public int promien=0;
	public int speed=1;
	Gracz()
	{
		super();
	}
	



	public int getpromien()
	{
		return promien;
	}
	public int getspeed()
	{
		return speed;
	}
	
	public void wgore() {
		// TODO Auto-generated method stub
		gora=true;
		dol=false;		
	}

	public void wdol() {
		// TODO Auto-generated method stub
		dol=true;
		gora=false;
	}

	public void wlewo() {
		// TODO Auto-generated method stub
		lewo=true;
		prawo=false;
	}

	public void wprawo() {
		// TODO Auto-generated method stub
		prawo=true;
		lewo=false;
	}
	
	public void idz(int i)
	{

		
		
		if (gora) {this.setBounds(this.getX(), this.getY()-wys*i, this.szer, this.wys);}
		else if (dol) {this.setBounds(this.getX(), this.getY()+wys*i, this.szer, this.wys);}
		if (prawo) {this.setBounds(this.getX()+szer*i, this.getY(), this.szer, this.wys);}
		else if (lewo) {this.setBounds(this.getX()-szer*i, this.getY(), this.szer, this.wys);}
	}

	public void setWysSzer(int wyskafla, int szerkafla) {
		// TODO Auto-generated method stub
		wys=wyskafla;
		szer=szerkafla;
	}

	public void stopwgore() {
		// TODO Auto-generated method stub
		gora=false;
	}

	public void stopwlewo() {
		// TODO Auto-generated method stub
		lewo=false;
	}

	public void stopwprawo() {
		// TODO Auto-generated method stub
		prawo=false;
	}

	public void stopwdol() {
		// TODO Auto-generated method stub
		dol=false;
	}



	@Override
	public InputMap getInputMapI(int whenInFocusedWindow) {
		// TODO Auto-generated method stub
		return getInputMap(whenInFocusedWindow);
	}



	@Override
	public ActionMap getActionMapI() {
		// TODO Auto-generated method stub
		return getActionMap();
	}



	@Override
	public Color getBackgroundColor() {
		// TODO Auto-generated method stub
		return this.getBackground();
	}



	@Override
	public void setBackgroundColor(Color color) {
		// TODO Auto-generated method stub
		this.setBackground(color);
	}

	
}

class Gracz1 extends Gracz
{


}
class Gracz2 extends Gracz
{


	
}
class Gracz3 extends Gracz
{

	
}
class Gracz4 extends Gracz
{

	
}