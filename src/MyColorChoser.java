import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSlider;




public class MyColorChoser extends MyChoser implements Cloneable, FocusListener, MouseMotionListener{

	
	Color c;
  JSlider[] RGBslider;
 JPanel pokaz;
 JPanel container;
private Object d;
JButton bu;
	 MyColorChoser()
	 {
		
		 super ();
		 inicjuj();

		
	 }
	private void inicjuj() {
		// TODO Auto-generated method stub
		container= new JPanel(new BorderLayout());
		 pokaz=new JPanel();
	//	pokaz.setMinimumSize(new Dimension(50,50));
		RGBslider = new JSlider[3];
		for (int i=0; i<3; i++)
		{
			 RGBslider[i]= new JSlider (0,255);
			 RGBslider[i].addFocusListener(this);
	         RGBslider[i].addMouseMotionListener(this);
	}
		
		container.add(RGBslider[0], BorderLayout.LINE_START);
		container.add(RGBslider[1], BorderLayout.CENTER);
		container.add(RGBslider[2], BorderLayout.LINE_END);
		
		c=new Color(128,128,128);
		pokaz.setBackground(c);
		add(container,BorderLayout.LINE_START);
		add(pokaz,BorderLayout.LINE_END);
		
		
	}

	Color getColor () {
		return c;
	}
	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub
		update(this);
		// RGBslider[0].setValue(0);
	}
	private void update(MyColorChoser myColorChoser) {
		// TODO Auto-generated method stub
		  
		       
		        int[] value = new int[3];
		      
		        for (int i = 0; i < 3; i++) {

		            value[i] = RGBslider[i].getValue();
		       
		        }
		        c= new Color (value[0],value[1],value[2]);
		        pokaz.setBackground(c);
		        
		        pokaz.repaint();
		            }
		
	
	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void focusGained(FocusEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void focusLost(FocusEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public MyChoser klonuj() {
		// TODO Auto-generated method stub
		 FileInputStream fileIn;
		  MyColorChoser klon = null;
		try {
			fileIn = new FileInputStream("MyColorChoser.ser");
			//fileIn = new FileInputStream(System.getProperty("user.dir")+"\\src\\tmp\\MyColorChoser.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
	          klon = (MyColorChoser) in.readObject();
	         in.close();
	         fileIn.close();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
         
		return klon;
	}
}
