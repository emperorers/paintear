import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.JPanel;


public abstract class MyChoser extends JPanel {
	
	static void makeprototype(Object obj, String src) {
		// TODO Auto-generated method stub
		try
	      {
			
			File wr = null;
			 wr = new File(src+".ser");

	         FileOutputStream fileOut =
	         new FileOutputStream(wr);  
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	        
			out.writeObject(obj);
	         out.close();
	         fileOut.close();
	      //   System.out.println("Saved in /tmp/"+src+".ser");
	      }catch(IOException i)
	      {
	          i.printStackTrace();
	      }	
	}
	abstract public MyChoser klonuj();
}
