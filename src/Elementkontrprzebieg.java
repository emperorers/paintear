import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.Timer;


public class Elementkontrprzebieg implements WindowListener, ActionListener {

	// Akcje- wykonawcy polecen Wzorzec Polecenie:
	public Action wgore, wdol, wlewo, wprawo, stopwgore,stopwdol,stopwlewo,stopwprawo, odpalspec;
	public JFrame frame; //menu
	public JP gra;		//plansza
	public Scenariusz scenario; //�r�d�o danych wykorzystywane w budowie gry
	public Budowniczy bud;		// abstrakcyjny budowniczy
	Elementkontrprzebieg()
	{
		 frame = new JFrame("PainTearMenu");
		 frame.setName("Menu");
		 scenario= new Scenariusz();
		 Menu menu =new Menu(frame, scenario);
	     frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	     frame.addWindowListener(this);
	     frame.getContentPane().add(menu);
	     frame.pack();
	     frame.setVisible(true);
	     bud = new BudZwykl(scenario);
	     wgore = new wgore();
	     wdol = new wdol();
	     wlewo=new wlewo();
	     wprawo=new wprawo();
	     stopwgore=new stopwgore();
	     stopwlewo=new stopwlewo();
	     stopwprawo = new stopwprawo();
	     stopwdol = new stopwdol();
	     odpalspec = new odpalspec();
	}
	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
		
		if (e.getComponent().getName().equals("Menu"));
		{
		frame= new JFrame("PainTear");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 frame.pack();
	     frame.setVisible(true);
	     budujALL(bud);
	     
		}
	}

	private void budujALL(Budowniczy budowniczy) {
		// TODO Auto-generated method stub
		budowniczy.budujplansze();
		for (int i=0; i<scenario.getLiczbagraczy(); i++)
		{
		budowniczy.budujgracza(i,wgore, wdol, wlewo, wprawo, stopwgore, stopwdol, stopwlewo, stopwprawo, odpalspec);
		}
		budowniczy.budujtimer(this);
		gra=budowniczy.getResult();
		frame.add(gra);
		frame.pack();
		frame.repaint();
	}
	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub	
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		 
			gra.losujbonus();
			gra.ruch();
			
			scenario.gameticks--;
			if (scenario.gameticks==0)
			{
				((Timer) arg0.getSource()).stop();
				punktuj();
				
			}
		
	}
	private void punktuj() {
		// TODO Auto-generated method stub
		int wygrana=0;
		int zwyciezca=0;
		 HashMap<Color,Integer> hm = new HashMap<Color, Integer>();
		 //ITERATOR
		for (Kafel o: gra.matrix2) {
			if (hm.get(o.getColor())==null)
			{
				hm.put(o.getColor(),  0);
			}
			else hm.put(o.getColor(), hm.get(o.getColor())+1);
			}
			
		
			for (int i=0; i<scenario.getLiczbagraczy(); i++)
			{
				if (hm.get(gra.gracze[i].getBackgroundColor())>wygrana) {
				//	System.out.println("ke?");
					wygrana=hm.get(gra.gracze[i].getBackgroundColor());
					zwyciezca=i+1;
					}
				gra.wyswietl("Gracz o numerze: "+(i+1)+" zdoby� "+hm.get(gra.gracze[i].getBackgroundColor())+"punktow!");
				
				System.out.println("Gracz o numerze: "+(i+1)+" zdoby� "+hm.get(gra.gracze[i].getBackgroundColor())+"punktow!");
			}
			gra.wyswietl("Zwyciezyl Gracz"+zwyciezca);
			System.out.println("Zwyciezyl Gracz"+zwyciezca);
	}

}

class wgore extends AbstractAction {
	
	public wgore() {super();}
    public void actionPerformed(ActionEvent e) {
    	//System.out.println("w gore");
    	((Gracz) e.getSource()).wgore();
    	
    }}
class wdol extends AbstractAction {
//	Gracz ha;
	public wdol() {super();}
    public void actionPerformed(ActionEvent e) {
  //  	System.out.println("w dol");
    //	ha=(Gracz) e.getSource();
    	((Gracz) e.getSource()).wdol();
    }}
class wlewo extends AbstractAction {
	//Gracz ha;
	public wlewo() {super();}
    public void actionPerformed(ActionEvent e) {
    //	System.out.println("w lewo");
    //	ha=((Gracz) e.getSource());
    	((Gracz) e.getSource()).wlewo();
    }}
class wprawo extends AbstractAction {
//	Gracz ha;
	public wprawo() {super();}
    public void actionPerformed(ActionEvent e) {
    //	System.out.println("w prawo");
    	//ha=(Gracz) e.getSource();
    	
    	((Gracz) e.getSource()).wprawo();
    }}
class stopwgore extends AbstractAction {
	//Gracz ha;
	public stopwgore() {super();}
    public void actionPerformed(ActionEvent e) {
    //	System.out.println("stop w gore");
    //	ha=(Gracz) e.getSource();
    	((Gracz) e.getSource()).stopwgore();
    }}
class stopwlewo extends AbstractAction {
	//Gracz ha;
	public stopwlewo() {super();}
    public void actionPerformed(ActionEvent e) {
    //	System.out.println("stopwlewo");
    //	ha=(Gracz) e.getSource();
    	((Gracz) e.getSource()).stopwlewo();
    }}
class stopwprawo extends AbstractAction {
//	Gracz ha;
	public stopwprawo() {super();}
    public void actionPerformed(ActionEvent e) {
    //	System.out.println("stopwprawo");
//    	ha=(Gracz) e.getSource();
    	((Gracz) e.getSource()).stopwprawo();
    }}
class stopwdol extends AbstractAction {
	//Gracz ha;
	public stopwdol() {super();}
    public void actionPerformed(ActionEvent e) {
    //	System.out.println("stopwdol");
    //	ha=(Gracz) e.getSource();
    	((Gracz) e.getSource()).stopwdol();
    }}
class odpalspec extends AbstractAction {
	//Gracz ha;
	public odpalspec() {super();}
    public void actionPerformed(ActionEvent e) {
    //	System.out.println("odpalspec");
    	
    }}

