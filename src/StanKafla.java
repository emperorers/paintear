import java.awt.Color;
import java.awt.Dimension;

//TO CO KAZDY STAN JAKOS IMPLEMENTOWAC POWINNIEN
public abstract class StanKafla {

	Dimension size;
	Color d=Color.CYAN;
	int licz=0;
	int lpunktow=0;
	public abstract void zamaluj(Color c);
	public abstract int punktuj();
	public abstract double miarkujgracza();
	public abstract int promien();
}

class Normal extends StanKafla //STAN TYPOWY 
{
	@Override
	public void zamaluj(Color c) {
		// TODO Auto-generated method stub
		d=c;
	}

	@Override
	public int punktuj() {
		// TODO Auto-generated method stub
		return 0;
	}




	@Override
	public double miarkujgracza() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int promien() {
		// TODO Auto-generated method stub
		return 0;
	}
}
class Halt extends StanKafla //STAN ZMIENIAJACy DZIALANIE ZAMALUJ
{
	
	Halt()
	{
		super();
		d=Color.BLACK;	
	}
	@Override
	public int punktuj() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void zamaluj(Color c) {
		// TODO Auto-generated method stub
		d=Color.BLACK;
		
	}
	@Override
	public double miarkujgracza() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int promien() {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
//POZOSTALE STANY, ODMIENNE DZIALA METODA miarkuj i promien
class BonusSpeedo extends Normal 
{
	BonusSpeedo()
	{
		super();
		d=Color.GREEN;
	}
	
	@Override
	public double miarkujgracza() {
		// TODO Auto-generated method stub
		return 1;
	}
	
}
class BonusPromien extends Normal
{
	BonusPromien()
	{
		super();
		d=Color.YELLOW;
	}
	public int promien()
	{
		
		return 1;
	}
	@Override
	public double miarkujgracza() {
		// TODO Auto-generated method stub
		return 0;
	}
	
}