import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class Menu extends JPanel implements ChangeListener{
	JButton[] Pocz; //dwa+ przycsiki pojawiajace sie na samym poczatku 
	JFrame Menuframe;
	Scenariusz scenario; 
	/*MyChoser - abstrakcyjna klasa rodzic dla MyColorChoser i MyKeyChoser
	 * Wzorzec prototyp*/
	MyChoser[][] ppp; 
	boolean a=true;

	Menu(JFrame frame, Scenariusz scen)
	{
		super();
		Menuframe=frame;
		scenario=scen;
		inicjalizuj();

	}


	
	private void inicjalizuj() {
		// TODO Auto-generated method stub
		//Huk, wiem ze tylko dwa, moze kiedys bedzie wiecej
		Pocz= new JButton[]{new JButton("Nowa Gra"),new JButton()};
		for (int i=0; i<Pocz.length; i++)
		{
			this.add(Pocz[i]);

		}

		Pocz[0].addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Menuframe.dispose();
			}});
		Pocz[1].addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
		    	wejdzwpodemenu(); 
			}});
		
		
		Pocz[1].setText("Ustawienia");
		
		
	}

	//Miejsce gdzie tworza sie parametry okreslajace "jak budowac"
	private void wejdzwpodemenu() {
		
		JButton Potwierdz= new JButton("OK");
		JButton Anuluj= new JButton("Anuluj"); //to do
		JButton Zapisz= new JButton("Zapisz Ustawienia na dysku"); //to do
		JButton Wczytaj= new JButton("Wczytaj Ustawienia z dysku"); //to do
		
		  //STEROWANIE ppp[0][i]
	      // KOLORY ppp[1][i]
		 // itych graczy
		ppp= new MyChoser[2][4];  


		Potwierdz.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				scenario.zapisz(ppp); //zapisujemy najwazniejsze ustawienia 
		    	Menuframe.dispose(); //konczymy z ta ramka
				
			}});
		
		JPanel Kolorsy=new JPanel();
		Kolorsy.setLayout(new BoxLayout(Kolorsy, BoxLayout.Y_AXIS));
		
		JPanel Buttonsy=new JPanel();
		Buttonsy.add(Potwierdz);
		Buttonsy.add(Anuluj);
		Buttonsy.add(Zapisz);
		Buttonsy.add(Wczytaj);
		
		JPanel Podemenu=new JPanel();
		JPanel Stermenu= new JPanel();
		Podemenu.setLayout(new BoxLayout(Podemenu, BoxLayout.Y_AXIS));

		  
		//Wzorzec prototyp: tworzymy pierwsze obiekty
		 ppp[1][0] = new MyColorChoser();		 
		 ppp[0][0] = new MyKeyChoser();
		 
		 //Tworzymy GLEBOKA kopie pierwszych obiektow 
	       MyChoser.makeprototype(ppp[1][0],"MyColorChoser");
	       MyChoser.makeprototype(ppp[0][0],"MyKeyChoser");
		
		Podemenu.setOpaque(true);
	
		JRadioButton Graczy2 = new JRadioButton("Dw�ch");
		JRadioButton Graczy3 = new JRadioButton("Trzech");
		JRadioButton Graczy4 = new JRadioButton("Czterech");
		
		
		ButtonGroup group = new ButtonGroup();
	    group.add(Graczy2);
	    group.add(Graczy3);
	    group.add(Graczy4);
	    
	    // Sluchacz radiobuttonow
	    class Wewnlistener implements ActionListener
		   {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				((AbstractButton) arg0.getSource()).setSelected(true);
				
				//prototyp, nazwa powinna byc kopiujPanelChooser
				//Fajny przyklad rowniez wzorca polecenia w ukrytych mech. swinga - actioncommand
				kopiujColorPanelChooser(arg0.getActionCommand(), ppp, Stermenu, Kolorsy);
				scenario.setLiczbagraczy(arg0.getActionCommand());
				
			}
			   
		   }
	   
	   Wewnlistener Sluchacz = new Wewnlistener();
	   Graczy2.addActionListener(Sluchacz);
	   Graczy2.setActionCommand("2");
	   Graczy3.addActionListener(Sluchacz);
	   Graczy3.setActionCommand("3");
	   Graczy4.addActionListener(Sluchacz);
	   Graczy4.setActionCommand("4");
	   
	   JPanel LiczbaGraczyPanel = new JPanel();
	   
	   LiczbaGraczyPanel.setBorder(BorderFactory.createTitledBorder(
               "Wybierz liczb� graczy"));
	   LiczbaGraczyPanel.add(Graczy2);
	   LiczbaGraczyPanel.add(Graczy3);
	   LiczbaGraczyPanel.add(Graczy4);
	   LiczbaGraczyPanel.setOpaque(true);
	   
       ppp[1][0].setBorder(BorderFactory.createTitledBorder("Wybierz kolor graczu 1"));
       ppp[0][0].setBorder(BorderFactory.createTitledBorder("Wybierz sterowanie graczu 1"));


       
       Podemenu.add(LiczbaGraczyPanel);
       Kolorsy.add(ppp[1][0]);
       Stermenu.add(ppp[0][0]);
       Podemenu.add(Stermenu);
       Podemenu.add(Kolorsy);
       Podemenu.add(Buttonsy);
       
       Menuframe.setContentPane(Podemenu);
       
	   Menuframe.pack();
       Menuframe.setVisible(true);

	}



	public void kopiujColorPanelChooser(String actionCommand, 
			 MyChoser[][] ppp, JPanel stermenu, JPanel kolorsy) {
		// TODO Auto-generated method stub
		
		//zaczyna sie od gracza2 //konczy sie na ostatnim graczu
		for (int i=scenario.getLiczbagraczy(); i<Integer.valueOf(actionCommand); i++)
		{
		//Sztandarowa komenda wzorca prototyp
		ppp[0][i]=ppp[0][0].klonuj();
		ppp[1][i]=ppp[1][0].klonuj();

		ppp[0][i].setBorder(BorderFactory.createTitledBorder("Wybierz sterowanie graczu "+String.valueOf(i+1)));
		ppp[1][i].setBorder(BorderFactory.createTitledBorder("Wybierz kolor graczu "+String.valueOf(i+1)));
	
		stermenu.add(ppp[0][i]);
		kolorsy.add(ppp[1][i]);
		}
		//od gracza ostatniego (zbednego) do aktualnej (zredukowanej) liczby graczy
		for (int i=scenario.getLiczbagraczy()-1; i>Integer.valueOf(actionCommand)-1; i--)
		{
			kolorsy.remove(ppp[1][i]);
			stermenu.remove(ppp[0][i]);
			ppp[1][i]=null;
			ppp[0][i]=null;
		}
		Menuframe.pack();
		
	}
	







	@Override
	public void stateChanged(ChangeEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	

}
