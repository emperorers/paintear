import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JComponent;


//WZORZEC STAN ZE STANEM WYDELEGOWANYM DO ODDZIELNYCH KLAS
public class Kafel extends JComponent{
	private StanKafla stan;
	Kafel()
	{
		stan= new Normal(); //POCZATKOWY STAN
	}

	public int punktuj(){return stan.punktuj();};
	public int getpromien(){return stan.promien();};
	public double miarkujgracza(){return stan.miarkujgracza();};
	public void zmientyp(StanKafla nowy){stan=nowy;};
	public void zamaluj (Color c) {stan.zamaluj(c);}
    public Color getColor() {return stan.d;}
    public void setDimension(int a, int b){stan.size=new Dimension(a,b);}

}