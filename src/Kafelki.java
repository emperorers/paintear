import java.util.Iterator;

//ITERATOR
public class Kafelki implements Iterable <Kafel>{
	private Kafel matrix[][];

	public Kafelki(int a, int b)
	{
		matrix= new Kafel[a][b];
		
	}

	@Override
	public Iterator<Kafel> iterator()
	{
		return new Iterator<Kafel>()
				{
					private int przed=0;
					@Override
					public boolean hasNext() {
						// TODO Auto-generated method stub
						return przed<size();
					}

					@Override
					public Kafel next() {
						// TODO Auto-generated method stub
						return get (przed++);
					}
			
				};
	}
	public int onesize()
	{
		return matrix.length;
	}
	public int size()
	{
		return matrix.length*matrix[0].length;
	}
	public Kafel get(int i)
	{
		return matrix[i/matrix.length][i%matrix[0].length];
	}
	public Kafel get(int i, int y)
	{
		return matrix[i][y];
	}
	public void set (int i, Kafel o)
	{
		matrix[i/matrix.length][i%matrix[0].length]=o;
	}
	public void set (int i, int y, Kafel o)
	{
		matrix[i][y]=o;
	}

	
}

   