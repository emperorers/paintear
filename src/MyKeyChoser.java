import java.awt.Dimension;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class MyKeyChoser extends MyChoser {
	Dimension r= new Dimension(50,20);
	String border;
	//JButton Sterowanie;
	Box up, down, left, right, special;
	JLabel upl, downl, leftl, rightl, speciall;
	String ups, downs, lefts, rights, specials;
	public JTextField upt, downt, leftt, rightt, specialt;
	public int[] stery;


	MyKeyChoser()
	{
		super();
		
		stery=new int[5];
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
	
		//this.setMinimumSize(new Dimension(200,200));
		this.setPreferredSize(new Dimension(200,135));
			ups   ="g�ra   ";
			downs ="d�    ";
			lefts ="lewo   ";
			rights="prawo  ";
		  specials="special";
		
	//	JButton Sterowanie = new JButton ("Ustaw");
		
		upt= new JTextField(5);
		downt= new JTextField(5);
		leftt= new JTextField(5);
		rightt= new JTextField(5);
		specialt= new JTextField(5);
		
		upt.setMaximumSize(upt.getPreferredSize());
		downt.setMaximumSize(downt.getPreferredSize());
		leftt.setMaximumSize(leftt.getPreferredSize());
		rightt.setMaximumSize(rightt.getPreferredSize());
		specialt.setMaximumSize(specialt.getPreferredSize());

		inicjujlistenery();
		
		up=new Box(BoxLayout.X_AXIS);
		up.setAlignmentX(0);
		down=new Box(BoxLayout.X_AXIS);
		down.setAlignmentX(0);
		left=new Box(BoxLayout.X_AXIS);
		left.setAlignmentX(0);
		right=new Box(BoxLayout.X_AXIS);
		right.setAlignmentX(0);
		special=new Box(BoxLayout.X_AXIS);
		special.setAlignmentX(0);
		
		upl= new JLabel(ups);
		upl.setMaximumSize(r);
		downl= new JLabel(downs);
		downl.setMaximumSize(r);
		leftl= new JLabel(lefts);
		leftl.setMaximumSize(r);
		rightl= new JLabel(rights);
		rightl.setMaximumSize(r);
		speciall= new JLabel(specials);
		speciall.setMaximumSize(r);
		
		up.add(upl);
		up.add(upt);
		
		down.add(downl);
		down.add(downt);
		
		left.add(leftl);
		left.add(leftt);
		
		right.add(rightl);
		right.add(rightt);
		
		special.add(speciall);
		special.add(specialt);
		
		add(up);
		add(down);
		add(left);
		add(right);
		add(special);
		

	
		
	}
	
	void inicjujlistenery()
	{   //SINGLETON 
		widthcontrol wc= widthcontrol.getinstance();
		upt.addKeyListener(wc);
		downt.addKeyListener(wc);
		leftt.addKeyListener(wc);
		rightt.addKeyListener(wc);
		specialt.addKeyListener(wc);
	}

	@Override
	public MyChoser klonuj() {
		// TODO Auto-generated method stub
		 FileInputStream fileIn;
		  MyKeyChoser klon = null;
		try {
			fileIn = new FileInputStream("MyKeyChoser.ser");
			//fileIn = new FileInputStream(System.getProperty("user.dir")+"\\src\\tmp\\MyKeyChoser.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
	          klon = (MyKeyChoser) in.readObject();
	         in.close();
	         fileIn.close();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
         
		return klon;
	}

}
