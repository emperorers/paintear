import java.awt.Color;

import javax.swing.ActionMap;
import javax.swing.InputMap;


//Dekoratory

public abstract class Dekogracz implements Graczi {

	Graczi gr;
	public Dekogracz(Graczi gracz) {
		// TODO Auto-generated constructor stub
		gr=gracz;
	}
	@Override
	public void setBounds(int i, int j, int wyskafla, int szerkafla) {
		// TODO Auto-generated method stub
		gr.setBounds(i, i, wyskafla, szerkafla);
	}

	@Override
	public void setWysSzer(int wyskafla, int szerkafla) {
		// TODO Auto-generated method stub
		gr.setWysSzer(wyskafla, szerkafla);
		
	}

	@Override
	public void setBackgroundColor(Color color) {
		// TODO Auto-generated method stub
		gr.setBackgroundColor(color);
	}

	@Override
	public InputMap getInputMapI(int whenInFocusedWindow) {
		// TODO Auto-generated method stub
		return gr.getInputMapI(whenInFocusedWindow);
	}

	@Override
	public ActionMap getActionMapI() {
		// TODO Auto-generated method stub
		return gr.getActionMapI();
	}
	



	@Override
	public int getX() {
		// TODO Auto-generated method stub
		return gr.getX();
	}

	@Override
	public int getWidth() {
		// TODO Auto-generated method stub
		return gr.getWidth();
	}

	@Override
	public int getHeight() {
		// TODO Auto-generated method stub
		return gr.getHeight();
	}

	@Override
	public Color getBackgroundColor() {
		// TODO Auto-generated method stub
		if (gr!=null)
		{
		return gr.getBackgroundColor();
		}
		else return Color.BLUE;
		
	}

	@Override
	public int getY() {
		// TODO Auto-generated method stub
		return gr.getY();
	}
	public int getpromien(){return gr.getpromien();}
	public int getspeed(){return gr.getspeed();}
	@Override
	public void idz(int i) {
		// TODO Auto-generated method stub
		gr.idz(i);
		
	}


	@Override
	public void setBackground(Color color) {
		// TODO Auto-generated method stub
		gr.setBackgroundColor(color);
		
	}
}
//KONKRETNE DEKORATORY
class expromien extends Dekogracz
{
	
	public expromien(Graczi gracz) {
		super(gracz);
		
		// TODO Auto-generated constructor stub
	}
	public int getpromien()
	{
	
		return super.getpromien()+1;
	}
	
}
class speed extends Dekogracz
{
	
	public speed(Graczi gracz) {
		super(gracz);
		
		// TODO Auto-generated constructor stub
	}
	public int getspeed()
	{
	
		return super.getspeed()+1;
	}
	
}

