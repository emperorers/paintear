import javax.swing.Action;


public abstract class Budowniczy {

	Scenariusz scenariusz;
	Budowniczy(Scenariusz scen)
	{
		scenariusz=scen;
	}

	abstract public void budujplansze();
	abstract public void budujtimer(Elementkontrprzebieg elementkontrprzebieg);
	abstract public JP getResult() ;

	public abstract void budujgracza(int i, Action wgore, Action wdol, Action wlewo,
			Action wprawo, Action stopwgore, Action stopwdol, Action stopwlewo,
			Action stopwprawo, Action odpalspec);
}
